#include <iostream>
using namespace std;
#define MAX_N 40
#define BASE (n)
int n;
int seq[MAX_N*(MAX_N+1)][MAX_N*(MAX_N+1)+1]; // seq[i][0] is the length of layer i
int h[MAX_N*MAX_N*MAX_N];
int max_t=2;
int inline fromNum1(int x)
{
    return x/(BASE*BASE);
}
int inline fromNum2(int x)
{
    return x%(BASE*BASE)/BASE;
}
int inline fromNum3(int x)
{
    return x % BASE;
}
int inline getNum(int a, int b, int c)
{
    if(a==b && b==c) return 7 << (3*a);
    if(a==b)return (3<<(3*b)) | (1<<(3*c));
    if(b==c)return (3<<(3*b)) | (1<<(3*a));
    return (1<<(3*a)) | (1<<(3*b)) | (1<<(3*c));
}
int inline getHashNum(int a, int b, int c)
{
    return a*BASE*BASE + b*BASE + c;
}
bool checkJoint(int u, int v, int w)
{
    return ((u & v) & w) == (u & v);
}
bool check(int ptr, int vIndex) // Check if seq[ptr] is OK with all seq[i] where i < ptr
{
    //for(int vIndex = 1; vIndex <= seq[ptr][0]; ++vIndex)
    {
        int v = seq[ptr][vIndex];
        for(int ptr0 = ptr-1; ptr0>=0; --ptr0)
        {
            for(int uIndex = 1; uIndex <= seq[ptr0][0]; ++uIndex)
            {
                int u = seq[ptr0][uIndex];
                for(int ptr1 = ptr0+1; ptr1<ptr; ++ptr1)
                {
                    bool found = false;
                    for(int wIndex = 1; wIndex <= seq[ptr1][0]; ++wIndex)
                    {
                        int w = seq[ptr1][wIndex];
                        if(checkJoint(u,v,w)) found = true;
                    }
                    if(!found) return false;
                }
            }
        }
    }
    return true;
}
void outputSeq(int ptr)
{
    for(int i=0; i<=ptr; ++i)
    {
        for(int j=1; j<=seq[i][0]; ++j)
        {
            int tmp = seq[i][j];
            cout << "(";
            for(int k = 0; k<n; ++k)
            {
                if((tmp & (1<<(3*k)))==(1<<(3*k)))
                    cout << k << " ";
                if((tmp & (3<<(3*k)))==(3<<(3*k)))
                    cout << k << " ";
                if((tmp & (7<<(3*k)))==(7<<(3*k)))
                    cout << k << " ";
            }
            cout << ")";
        }
        cout << endl;
    }
}
void search(int ptr)
{
    if(ptr+1>=max_t)
    {
        cout << "Max Length updated: " << max_t << " to " << ptr + 1 << endl;
        outputSeq(ptr);
        max_t = ptr+1;
    }
    for(int i=0;i<n;++i) // First try expand next one seq[ptr] with 
        for(int j=i;j<n;++j)
            for(int l=j;l<n;++l)
                if(!h[getHashNum(i,j,l)])
                {
                    seq[ptr+1][0] = 1;
                    seq[ptr+1][1] = getNum(i,j,l);
                    if(check(ptr+1, 1))
                    {
                        h[getHashNum(i,j,l)] = 1;
                        search(ptr+1);
                        h[getHashNum(i,j,l)] = 0;
                    }
                }
    if(ptr) // Same line expansion
    {
        for(int i=0;i<n;++i)
        for(int j=i;j<n;++j)
        for(int l=j;l<n;++l)
            if(!h[getHashNum(i,j,l)])
            {
                ++seq[ptr][0];
                seq[ptr][seq[ptr][0]] = getNum(i,j,l);
                if(check(ptr, seq[ptr][0]))
                {
                    h[getHashNum(i,j,l)] = 1;
                    search(ptr);
                    h[getHashNum(i,j,l)] = 0;
                }
                --seq[ptr][0];
            }
    }
}
int main()
{
    cin >> n;
    seq[0][0]=1;
    seq[0][1] = getNum(0,0,0);
    h[getHashNum(0,0,0)]=1;
    search(0);
    h[getHashNum(0,0,0)]=0;
    seq[0][1] = getNum(0,0,1);
    h[getHashNum(0,0,1)]=1;
    search(0);
    h[getHashNum(0,0,1)]=0;
    seq[0][1] = getNum(0,1,2);
    h[getHashNum(0,1,2)]=1;
    search(0);
    h[getHashNum(0,1,2)]=0;
    return 0;
}